﻿using MeetMiLlajta.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MeetMiLlajta.Arriendo.CRUD
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArriendoCrud : ContentPage
    {
        public ArriendoCrud()
        {
            InitializeComponent();

            
            listViewArriendos.RowHeight = 100;
            listViewArriendos.ItemTemplate = new DataTemplate(typeof(ArriendoCell));
            listViewArriendos.ItemSelected += listViewArriendos_ItemSelected;


        }

        //protected override void OnAppearing()
        //{
        //    base.OnAppearing();
        //    using (var datos = new BaseDatos.DataAccess())
        //    {
        //        listViewArriendos.ItemsSource = datos.GetArriendos();
        //    }
        //}



        private async void agregarButton_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(UbicacionEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar ubicacion", "Aceptar");
                UbicacionEntry.Focus();
                return;
            }

            if (string.IsNullOrEmpty(nombresEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar nombres", "Aceptar");
                nombresEntry.Focus();
                return;
            }

            if (string.IsNullOrEmpty(apellidosEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar apellidos", "Aceptar");
                apellidosEntry.Focus();
                return;
            }

            if (string.IsNullOrEmpty(telefonoEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar telefono", "Aceptar");
                telefonoEntry.Focus();
                return;
            }

            BaseDatos.Arriendo arriendoI = new BaseDatos.Arriendo
            {
                Activo = activoSwitch.IsToggled,
                Ubicacion = UbicacionEntry.Text,
                Nombres = nombresEntry.Text,
                Apellidos = apellidosEntry.Text,
                FechaPublicacion = fechaPublicacionDatePicker.Date,
                Numero = int.Parse(telefonoEntry.Text)
            };
            using (var datos = new BaseDatos.DataAccess())
            {
                datos.InsertArriendo(arriendoI);
                listViewArriendos.ItemsSource = datos.GetArriendos();
                
            }

            
            await DisplayAlert("Mensaje", "Arriendo agregado", "Aceptar");


        }

        private void listViewArriendos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Navigation.PushAsync(new EditArriendo((BaseDatos.Arriendo)e.SelectedItem));
        }

        private void BtnVolver_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new PerfilUsuario());
        }
    }
}