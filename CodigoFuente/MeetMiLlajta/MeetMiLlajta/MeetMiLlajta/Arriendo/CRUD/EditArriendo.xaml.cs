﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MeetMiLlajta.Arriendo.CRUD
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EditArriendo : ContentPage
	{
        private BaseDatos.Arriendo arriendo;
		public EditArriendo (BaseDatos.Arriendo arriendo)
		{
			InitializeComponent ();

            this.arriendo = arriendo;

            
            

            UbicacionEntry.Text = arriendo.Ubicacion;
            nombresEntry.Text = arriendo.Nombres;
            apellidosEntry.Text = arriendo.Apellidos;
            telefonoEntry.Text = arriendo.Numero.ToString();
            activoSwitch.IsToggled = arriendo.Activo;

		}

        private async void editButton_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(nombresEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar nombres", "Aceptar");
                nombresEntry.Focus();
                return;
            }

            if (string.IsNullOrEmpty(apellidosEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar apellidos", "Aceptar");
                apellidosEntry.Focus();
                return;
            }

            if (string.IsNullOrEmpty(UbicacionEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar ubicacion", "Aceptar");
                UbicacionEntry.Focus();
                return;
            }
            if (string.IsNullOrEmpty(telefonoEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar telefono", "Aceptar");
                telefonoEntry.Focus();
                return;
            }
            arriendo.Activo = activoSwitch.IsToggled;
            arriendo.Ubicacion = UbicacionEntry.Text;
            arriendo.Nombres = nombresEntry.Text;
            arriendo.Apellidos = apellidosEntry.Text;
            arriendo.FechaPublicacion = fechaPublicacionDatePicker.Date;
            arriendo.Numero = int.Parse(telefonoEntry.Text);
            using (var datos = new BaseDatos.DataAccess())
            {
                datos.UpdateArriendo(arriendo);
            }

            await DisplayAlert("Confirmación", "Arriendo actualizado correctamente", "Aceptar");
            await Navigation.PopAsync();

        }

        private async void eliminarButton_Clicked(object sender, EventArgs e)
        {
            var rta = await DisplayAlert("Confirmación", "¿Desea borrar el Arriendo?", "Si", "No");
            if (!rta) return;

            using (var datos = new BaseDatos.DataAccess())
            {
                datos.DeleteArriendo(arriendo);
            }

            await DisplayAlert("Confirmación", "Arriendo borrado correctamente", "Aceptar");
            //await Navigation.PushAsync(new HomePage());
            await Navigation.PopAsync();

        }

        private void listViewArriendos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            
        }
    }
}