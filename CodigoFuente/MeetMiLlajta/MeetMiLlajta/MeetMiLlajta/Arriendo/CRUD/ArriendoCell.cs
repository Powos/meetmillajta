﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MeetMiLlajta.Arriendo.CRUD
{
    public class ArriendoCell: ViewCell
    {
        public ArriendoCell()
        {
            var IDArriendoLabel = new Label
            {
                HorizontalTextAlignment = TextAlignment.End,
                HorizontalOptions = LayoutOptions.Start,
                FontSize = 20,
                FontAttributes = FontAttributes.Bold,

            };
            IDArriendoLabel.SetBinding(Label.TextProperty, new Binding("IDArriendo"));

            var ubicacionLabel = new Label
            {
                FontSize = 20,
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.StartAndExpand
            };

            ubicacionLabel.SetBinding(Label.TextProperty, new Binding("Ubicacion"));


            var nombreCompetoLabel = new Label
            {
                FontSize = 20,
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.StartAndExpand
            };

            nombreCompetoLabel.SetBinding(Label.TextProperty, new Binding("Nombres"));

            var apellidosLabel = new Label
            {
                FontSize = 20,
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.StartAndExpand
            };

            apellidosLabel.SetBinding(Label.TextProperty, new Binding("Apellidos"));



            var fechaPublicacionLabel = new Label
            {
                HorizontalOptions = LayoutOptions.StartAndExpand,
                FontSize = 20,
            };

            fechaPublicacionLabel.SetBinding(Label.TextProperty, new Binding("FechaPublicacion"));

            var telefLabel = new Label
            {
                HorizontalTextAlignment = TextAlignment.End,
                FontSize = 20,
                HorizontalOptions = LayoutOptions.StartAndExpand
            };

            telefLabel.SetBinding(Label.TextProperty, new Binding("Numero"));

            var activoSwitch = new Switch
            {
                IsEnabled = false,
                HorizontalOptions = LayoutOptions.End
            };

            activoSwitch.SetBinding(Switch.IsToggledProperty, new Binding("Activo"));

            var line1 = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Children = {
                    IDArriendoLabel, nombreCompetoLabel,ubicacionLabel
                },
            };

            var line2 = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Children = {
                    fechaPublicacionLabel, telefLabel, activoSwitch
                },
            };
            View = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                Children = {
                    line1, line2,
                },
            };



        }
    }
}
