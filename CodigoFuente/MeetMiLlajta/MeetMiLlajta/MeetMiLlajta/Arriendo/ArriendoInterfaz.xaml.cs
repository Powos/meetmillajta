﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MeetMiLlajta.Arriendo
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ArriendoInterfaz : ContentPage
	{
		public ArriendoInterfaz()
		{
			InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void BtnCalificar_ClickedAsync(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new CalificarArriendo());
        }

        private void BtnVolver_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new BusquedadeArriendoInterfaz());
        }


        private void BtnContactar_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new VisitaArriendo());
        }

        private void Button_Clicked(object sender, EventArgs e)
        {

        }

        private void BtnVerMapa_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new MapaArriendos());
        }
    }
}