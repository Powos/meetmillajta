﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MeetMiLlajta.Arriendo
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ValidarArriendo : ContentPage
	{
		public ValidarArriendo ()
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void LvwArriendos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            DisplayAlert("Revision de Arriendo", "¿Aceptar y Agregar Arriendo?", "Si","No");
        }
    }
}