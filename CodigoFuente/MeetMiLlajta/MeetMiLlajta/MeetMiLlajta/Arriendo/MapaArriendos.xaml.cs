﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace MeetMiLlajta.Arriendo
{

    public partial class MapaArriendos : ContentPage
    {
        public MapaArriendos()
        {
            InitializeComponent(); NavigationPage.SetHasNavigationBar(this, false);

            mapaA.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(-17.331765, -66.226160), Distance.FromMiles(0.5)));


            var position1 = new Position(-17.337618, -66.224837);
          

            var pin1 = new Pin
            {
                Type = PinType.Place,
                Position = position1,
                Label = "Arriendo 1",
                Address = "Arriendo 1 Ejemplo"
            };

           

            mapaA.Pins.Add(pin1);
           
        }

        private void BtnVolver_Clicked(object sender, EventArgs e)
        {
            CarouselPage CRP = new CarouselPage();
            Application.Current.MainPage = CRP;
            CRP.Children.Add(new MenuPrincipal.MenuCarouselArriendo());
            CRP.Children.Add(new MenuPrincipal.MenuCarouselLugares());
            CRP.Children.Add(new MenuPrincipal.MenuCarouselRutas());
        }
    }
}