﻿using Plugin.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MeetMiLlajta.Arriendo
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VisitaArriendo : ContentPage
    {
        public VisitaArriendo()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            dtpFechaVisita.MinimumDate = DateTime.Now;
        }

        private void BtnEnviar_Clicked(object sender, EventArgs e)
        {
            if (entNombre.Text != string.Empty && dtpFechaVisita.Date != null)
            {
                entNombre.Text?.Trim();
                var emailMessenger = CrossMessaging.Current.EmailMessenger;
                if (emailMessenger.CanSendEmail)
                {

                    var email = new EmailMessageBuilder()
                          .To("to.lucmn4@gmail.com")
                          .Cc("cc.lucmn4@gmail.com")
                          .Bcc(new[] { "lucmn4@gmail.com", "lucmn4@gmail.com" })
                          .Subject("Aviso visita para ver arriendo")
                          .Body("Estimado usuario, le avisamos que el interesado " + entNombre.Text + " agendó una visita para el día " + dtpFechaVisita.Date.Day.ToString() + "/" + dtpFechaVisita.Date.Month.ToString() + "/" + dtpFechaVisita.Date.Year.ToString())
                          .Build();

                    emailMessenger.SendEmail(email);
                }

            }
            else
            {
                DisplayAlert("Error", "Rellene los datos por favor", "Ok");
            }
        }

        private void BtnCancelar_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new ArriendoInterfaz());
        }
    }
}