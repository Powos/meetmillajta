﻿using MeetMiLlajta.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MeetMiLlajta.Arriendo
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BusquedadeArriendoInterfaz : ContentPage
    {
        int invi;
        private bool registarme = false;
        public BusquedadeArriendoInterfaz()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
        public BusquedadeArriendoInterfaz(int invitado)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            invi = invitado;
            if (invi == 1)
            {
                LvwBusquedaArriendos.IsEnabled = false;
            }
        }

        async void OnTapped(object sender, EventArgs e)
        {
            var answer = await DisplayAlert("Querido Invitado", "Recuerda que para ver todo el contenido deves de estar registrado", "Registrarme", "Salir");
            if (answer)
            {
                registarme = true;
                BotonSeleccionado();
            }
            
        }

        private void BotonSeleccionado()
        {
            if (registarme)
            {
                Application.Current.MainPage = new NavigationPage(new Registrar());
            }
        }

        async void LvwBusquedaArriendos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (LvwBusquedaArriendos.SelectedItem != null)
            {
                ArriendoInterfaz detailPage = new ArriendoInterfaz();
                await Navigation.PushModalAsync(detailPage);
            }
        }

        private void BtnCancelar_Clicked(object sender, EventArgs e)
        {
            if (invi == 1)
            {
                CarouselPage CRP = new CarouselPage();
                Application.Current.MainPage = CRP;
                CRP.Children.Add(new MenuPrincipal.MenuCarouselArriendo(1));
                CRP.Children.Add(new MenuPrincipal.MenuCarouselLugares(1));
                CRP.Children.Add(new MenuPrincipal.MenuCarouselRutas(1));
            }
            else
            {
                CarouselPage CRP = new CarouselPage();
                Application.Current.MainPage = CRP;
                CRP.Children.Add(new MenuPrincipal.MenuCarouselArriendo());
                CRP.Children.Add(new MenuPrincipal.MenuCarouselLugares());
                CRP.Children.Add(new MenuPrincipal.MenuCarouselRutas());
            }
        }
    }
}