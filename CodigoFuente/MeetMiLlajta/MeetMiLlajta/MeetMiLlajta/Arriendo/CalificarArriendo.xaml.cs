﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MeetMiLlajta.Arriendo
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CalificarArriendo : ContentPage
	{
		public CalificarArriendo ()
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void BtnVolverCalif_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new ArriendoInterfaz());
        }

        private void BtnEnviarCalif_Clicked(object sender, EventArgs e)
        {
            DisplayAlert("Review enviada", "Gracias por ayudar a mejorar la App, nos servirá para ofrecer lo mejor", "Aceptar");
        }
    }
}