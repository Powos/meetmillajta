﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQLite.Net;
using Xamarin.Forms;

namespace MeetMiLlajta.BaseDatos
{
    public class DataAccess : IDisposable
    {
        private SQLiteConnection connection;

        public DataAccess()
        {
            var config = DependencyService.Get<IConfig>();
            connection = new SQLiteConnection(config.Plataforma,
            System.IO.Path.Combine(config.DirectorioDB, "MeetMiLlajta.db3"));
            connection.CreateTable<Arriendo>();
        }
        public void InsertArriendo(Arriendo arriendo)
        {
            connection.Insert(arriendo);
        }

        public void UpdateArriendo(Arriendo arriendo)
        {
            connection.Update(arriendo);
        }

        public void DeleteArriendo(Arriendo arriendo)
        {
            connection.Delete(arriendo);
        }
        public Arriendo GetArriendo(int IDArriendo)
        {
            return connection.Table<Arriendo>().FirstOrDefault(c => c.IDArriendo == IDArriendo);
        }

        public List<Arriendo> GetArriendos()
        {
            return connection.Table<Arriendo>().OrderBy(c => c.Apellidos).ToList();
        }


        public void Dispose()
        {
            connection.Dispose();
        }
    }
}
