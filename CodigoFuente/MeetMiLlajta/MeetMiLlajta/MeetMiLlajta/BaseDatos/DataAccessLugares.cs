﻿using System;
using SQLite.Net;
using Xamarin.Forms;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace MeetMiLlajta.BaseDatos
{
    class DataAccessLugares : IDisposable
    {
        private SQLiteConnection connection;

        public DataAccessLugares()
        {
            var config = DependencyService.Get<IConfig>();
            connection = new SQLiteConnection(config.Plataforma, Path.Combine(config.DirectorioDB, "MeetMiLlajta.db3"));
            connection.CreateTable<LugaresInteres>();
        }

        public void InsertLugarInteres(LugaresInteres Lugares) {

            connection.Insert(Lugares);
        }

        public void UpdateLugarInteres(LugaresInteres Lugares)
        {

            connection.Update(Lugares);
        }

        public void DeleteLugarInteres(LugaresInteres Lugares)
        {

            connection.Delete(Lugares);
        }

        public LugaresInteres getLugarInteres(int IdLugarInteres)
        {
            return connection.Table<LugaresInteres>().FirstOrDefault(c =>c.IdLugarInteres == IdLugarInteres);
        }

        public List<LugaresInteres> getLugaresInteres()
        {
            return connection.Table<LugaresInteres>().ToList();
        }


        public void Dispose()
        {
            connection.Dispose();
        }
    }
}
