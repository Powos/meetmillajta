﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MeetMiLlajta.BaseDatos
{
    class LugarInteresCell : ViewCell
    {
        public LugarInteresCell()
        {
            var IdLugarInteres = new Label
            {
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Start
            };
            IdLugarInteres.SetBinding(Label.TextProperty, new Binding("IdLugarInteres"));

            var Nombre = new Label
            {
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Fill
            };
            Nombre.SetBinding(Label.TextProperty, new Binding("Nombre"));

            var Tipo = new Label
            {
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Fill
            };
            Tipo.SetBinding(Label.TextProperty, new Binding("Tipo"));

            var Latitud = new Label
            {
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Fill
            };
            Latitud.SetBinding(Label.TextProperty, new Binding("Latitud"));

            var Longitud = new Label
            {
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Fill
            };
            Longitud.SetBinding(Label.TextProperty, new Binding("Longitud"));

            View = new StackLayout
            {
                Children = { IdLugarInteres, Nombre, Tipo, Latitud, Longitud },
                Orientation = StackOrientation.Horizontal
            };
        }
    }
}

