﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net.Attributes;

namespace MeetMiLlajta.BaseDatos
{
    public class Arriendo
    {
        [PrimaryKey, AutoIncrement]
        public int IDArriendo { get; set; }
        public string Ubicacion { get; set; }
        public string Nombres { get; set; }

        public string Apellidos { get; set; }

        public int Numero { get; set; }
        public bool Activo { get; set; }

        public DateTime FechaPublicacion { get; set; }


       

        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3},{4}", IDArriendo, Ubicacion,Nombres,Apellidos, FechaPublicacion);
        }
    }
}
