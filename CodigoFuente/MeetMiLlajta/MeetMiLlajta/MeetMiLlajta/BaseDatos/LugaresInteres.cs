﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net.Attributes;

namespace MeetMiLlajta.BaseDatos
{
    public class LugaresInteres
    {
        [PrimaryKey,AutoIncrement]
        public int IdLugarInteres { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Tipo { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }

        public override string ToString()
        {
            return Latitud + "," + Longitud;
        }
    }
}
