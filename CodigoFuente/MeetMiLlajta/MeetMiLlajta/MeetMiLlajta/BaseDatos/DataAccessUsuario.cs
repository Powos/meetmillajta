﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using SQLite.Net;
using Xamarin.Forms;

namespace MeetMiLlajta.BaseDatos
{
    class DataAccessUsuario : IDisposable
    {
        private SQLiteConnection connection;
        public DataAccessUsuario()
        {
            var config = DependencyService.Get<IConfig>();
            connection = new SQLiteConnection(config.Plataforma, 
                Path.Combine(config.DirectorioDB, "MeetMiLlajta.db3"));
            connection.CreateTable<Usuario>();
        }
        public void InsertUsuario(Usuario usuario)
        {
            connection.Insert(usuario);
        }

        public void UpdateUsuario(Usuario usuario)
        {
            connection.Update(usuario);
        }

        public Usuario GetUsuario(string nombreUsuario)
        {
            return connection.Table<Usuario>().FirstOrDefault(c => c.NombreUsuario == nombreUsuario);
        }
        public void Dispose()
        {
            connection.Dispose();
        }
    }
}
