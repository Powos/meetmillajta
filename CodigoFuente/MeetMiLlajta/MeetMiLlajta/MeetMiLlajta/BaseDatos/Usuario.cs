﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite.Net.Attributes;

namespace MeetMiLlajta.BaseDatos
{
    public class Usuario
    {
        [PrimaryKey,AutoIncrement]
        public int IdUsuario { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string NombreUsuario { get; set; }
        public string CorreoUsuario { get; set; }
        public string Rol { get; set; }
        public string Contrasenha { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4}", IdUsuario, Nombres
                , Apellidos, NombreUsuario, CorreoUsuario);
        }


    }
}
