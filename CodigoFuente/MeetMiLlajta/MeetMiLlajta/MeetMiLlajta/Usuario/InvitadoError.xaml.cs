﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MeetMiLlajta.Usuario
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InvitadoError : ContentPage
	{
		public InvitadoError ()
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void BtnRegistrar_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new Registrar());
        }

        private void BtnAtras_Clicked(object sender, EventArgs e)
        {
            CarouselPage CRP = new CarouselPage();
            Application.Current.MainPage = CRP;
            CRP.Children.Add(new MenuPrincipal.MenuCarouselArriendo(1));
            CRP.Children.Add(new MenuPrincipal.MenuCarouselLugares(1));
            CRP.Children.Add(new MenuPrincipal.MenuCarouselRutas(1));
        }

        private void BtnLogin_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new Login());
        }
    }
}