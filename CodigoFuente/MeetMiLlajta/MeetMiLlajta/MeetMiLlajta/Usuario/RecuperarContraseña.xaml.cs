﻿using Plugin.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MeetMiLlajta.Usuario
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecuperarContraseña : ContentPage
    {
        public RecuperarContraseña()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void BtnCancelar_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new Login());
        }

        private void BtnEnviar_Clicked(object sender, EventArgs e)
        {
            var emailMessenger = CrossMessaging.Current.EmailMessenger;
            if (emailMessenger.CanSendEmail)
            {
                var email = new EmailMessageBuilder()
               .To("to." + txtCorreo.Text)
               .Cc("cc.diegoalejandrolucas@gmail.com")
               .Bcc(new[] { "bbc1.diegoalejandrolucas@gmail.com" })
               .Subject("Restaurar Contraseña")
               .Body("Mensaje para confirmar su restauracion de contraseña")
               .Build();
            }
        }
    }
}