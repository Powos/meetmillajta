﻿using Plugin.Messaging;
using System;
using System.Collections.Generic;
using System.IO;
using SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SQLite.Net;
using MeetMiLlajta.BaseDatos;

namespace MeetMiLlajta.Usuario
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Registrar : ContentPage
	{

        public Registrar ()
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void BtnCancelar_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new Login());
        }

        private async void BtnRegistrar_Clicked(object sender, EventArgs e)
        {

            if (txtNombres.Text!=string.Empty && 
                txtApellidos.Text!=string.Empty && 
                txtNombreUsuario.Text != string.Empty &&
                txtCorreo.Text != string.Empty &&
                txtContraseña.Text != string.Empty &&
                txtConfContraseña.Text != string.Empty)
            {
                txtNombres.Text.Trim();
                txtApellidos.Text.Trim();
                txtCorreo.Text.Trim();
                txtNombreUsuario.Text.Trim();
                if (Validaciones.ValidacionesUsuario.ValidarNombreCompleto(txtNombres.Text,txtApellidos.Text))
                {
                    if (Validaciones.ValidacionesUsuario.ValidarCorreo(txtCorreo.Text))
                    {
                        if (Validaciones.ValidacionesUsuario.VerificarContraseña(txtContraseña.Text, txtConfContraseña.Text))
                        {
                            var emailMessenger = CrossMessaging.Current.EmailMessenger;
                            if (emailMessenger.CanSendEmail)
                            {
                                var email = new EmailMessageBuilder()
                               .To("to." + txtCorreo.Text)
                               .Cc("cc.diegoalejandrolucas@gmail.com")
                               .Bcc(new[] { "bbc1.diegoalejandrolucas@gmail.com" })
                               .Subject("Confirmar Registro")
                               .Body("Mensaje para confirmar su registro de usuario en la aplicacion")
                               .Build();
                            }

                            BaseDatos.Usuario usuario = new BaseDatos.Usuario
                            {
                                Nombres = txtNombres.Text,
                                Apellidos = txtApellidos.Text,
                                NombreUsuario = txtNombreUsuario.Text,
                                Contrasenha = txtContraseña.Text,
                                Rol = "User"
                            };

                            using (var datos = new BaseDatos.DataAccessUsuario())
                            {
                                datos.InsertUsuario(usuario);
                            }

                            Sesion.Sesion.nombres = txtNombres.Text;
                            Sesion.Sesion.apellidos = txtApellidos.Text;
                            Sesion.Sesion.nombreUsuario = txtNombreUsuario.Text;
                            Sesion.Sesion.correo = txtCorreo.Text;
                            Sesion.Sesion.contraseña = txtContraseña.Text;
                            //Sesion.Sesion.rol = "User";
                            Application.Current.MainPage = new NavigationPage(new Login());
                        }
                        else
                        {
                            await DisplayAlert("Error", "Las contraseñas no coinciden", "Ok");
                        }
                    }
                    else
                    {
                        await DisplayAlert("Error", "El correo introducido tiene el formato incorrecto", "Ok");
                    }
                }
                else
                {
                    await DisplayAlert("Error", "El Formato de los nombres y/o apellidos es incorecto", "Ok");
                }     
            }
            else
            {
                await DisplayAlert("Error", "Debe llenar todos los datos", "Ok");
            }
            //Application.Current.MainPage = new NavigationPage(new Registrar());
        }
    }
}