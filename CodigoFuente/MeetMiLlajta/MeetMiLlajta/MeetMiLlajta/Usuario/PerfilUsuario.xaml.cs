﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MeetMiLlajta.Usuario
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PerfilUsuario : ContentPage
	{
		public PerfilUsuario ()
		{
			InitializeComponent();
            txtNombres.Text = Sesion.Sesion.nombres;
            txtApellidos.Text = Sesion.Sesion.apellidos;
            txtCorreo.Text = Sesion.Sesion.correo;
            txtNombreUsuario.Text = Sesion.Sesion.nombreUsuario;

            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void BtnCambiarContraseña_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new RecuperarContraseña());
        }

        private void BtnPrincipal_Clicked(object sender, EventArgs e)
        {
            CarouselPage CRP = new CarouselPage();
            Application.Current.MainPage = CRP;
            CRP.Children.Add(new MenuPrincipal.MenuCarouselArriendo());
            CRP.Children.Add(new MenuPrincipal.MenuCarouselLugares());
            CRP.Children.Add(new MenuPrincipal.MenuCarouselRutas());
        }

        private void BtnAdministrarLugaresInteres_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new LugaresInteres.CRUD.LugaresInteresAdministrar());
        }

        private void BtnAdministrarArriendos_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new Arriendo.CRUD.ArriendoCrud());
        }
    }
}