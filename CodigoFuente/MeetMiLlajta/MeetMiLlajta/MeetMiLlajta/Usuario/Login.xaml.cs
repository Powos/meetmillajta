﻿using System;
using System.IO;
using MeetMiLlajta.BaseDatos;
using SQLite;
using SQLite.Net;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MeetMiLlajta.Usuario
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Login : ContentPage
	{

        public Login ()
		{
			InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void BtnLogin_Clicked(object sender, EventArgs e)
        {

            if (txtNombreUsuario.Text!=string.Empty && txtContraseña.Text!=string.Empty)
            {
                using (var datos = new BaseDatos.DataAccessUsuario())
                {
                    BaseDatos.Usuario usuario = datos.GetUsuario(txtNombreUsuario.Text);
                    if (usuario != null)
                    {
                        Sesion.Sesion.nombreUsuario = usuario.NombreUsuario;
                        Sesion.Sesion.contraseña = usuario.Contrasenha;
                        Sesion.Sesion.correo = usuario.CorreoUsuario;
                        Sesion.Sesion.nombres = usuario.Nombres;
                        Sesion.Sesion.apellidos = usuario.Apellidos;
                        Sesion.Sesion.rol = usuario.Rol;
                    }
                }
                if (txtNombreUsuario.Text.Equals(Sesion.Sesion.nombreUsuario) && txtContraseña.Text.Equals(Sesion.Sesion.contraseña))
                {
                    Application.Current.MainPage = new NavigationPage(new PerfilUsuario());
                }
                else
                {
                    DisplayAlert("Error", "El usuario y/o la contraseña son incorrectos","Ok");
                }
            }
            else
            {
               DisplayAlert("Error", "Debe llenar los datos requeridos", "Ok");
            }
        }

        private void BtnCancelar_Clicked(object sender, EventArgs e)
        {
           //la aplicacion se sale
        }

        private void BtnInvitado_Clicked(object sender, EventArgs e)
        {
            CarouselPage CRP = new CarouselPage();
            Application.Current.MainPage = CRP;
            CRP.Children.Add(new MenuPrincipal.MenuCarouselArriendo(1));
            CRP.Children.Add(new MenuPrincipal.MenuCarouselLugares(1));
            CRP.Children.Add(new MenuPrincipal.MenuCarouselRutas(1));
        }

        private void BtnOlvidarContraseña_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new RecuperarContraseña());
        }

        private void BtnRegistrar_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new Registrar());
        }
    }
}