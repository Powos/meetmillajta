﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MeetMiLlajta.LugaresInteres
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LugaresInferfaz : ContentPage
	{
		public LugaresInferfaz ()
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void BtnVolver_Clicked(object sender, EventArgs e)
        {
            CarouselPage CRP = new CarouselPage();
            Application.Current.MainPage = CRP;
            CRP.Children.Add(new MenuPrincipal.MenuCarouselArriendo());
            CRP.Children.Add(new MenuPrincipal.MenuCarouselLugares());
            CRP.Children.Add(new MenuPrincipal.MenuCarouselRutas());
        }
    }
}