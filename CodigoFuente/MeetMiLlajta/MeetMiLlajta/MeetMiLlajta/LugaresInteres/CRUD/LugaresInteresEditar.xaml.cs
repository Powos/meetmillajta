﻿using MeetMiLlajta.BaseDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MeetMiLlajta.LugaresInteres.CRUD
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LugaresInteresEditar : ContentPage
	{
        private BaseDatos.LugaresInteres lugares;
		public LugaresInteresEditar (BaseDatos.LugaresInteres lugares)
		{
			InitializeComponent ();
            this.lugares = lugares;

            NombreLugar.Text = lugares.Nombre;
            Descripcion.Text = lugares.Descripcion;
            Tipo.Text = lugares.Tipo;
            Latidud.Text = lugares.Latitud.ToString();
            Longitud.Text = lugares.Longitud.ToString();

		}

        public async void BtnCambiar_Clicked(object sender, EventArgs e)
        {
            Validaciones();

            BaseDatos.LugaresInteres lugar = new BaseDatos.LugaresInteres
            {
                IdLugarInteres = this.lugares.IdLugarInteres,
                Nombre = NombreLugar.Text,
                Descripcion = Descripcion.Text,
                Tipo = Tipo.Text,
                Latitud = Latidud.Text,
                Longitud = Longitud.Text
            };

            using (var datos = new DataAccessLugares())
            {
                datos.UpdateLugarInteres(lugar);
            }

            await DisplayAlert("Confirmacion", "Lugar Seleccionado Actualizado Correctamente", "Aceptar");
            Application.Current.MainPage = new NavigationPage(new LugaresInteresAdministrar());
        }

        public async void BtnBorrar_Clicked(object sender, EventArgs e)
        {
            var rta = await DisplayAlert("Confirmacion","¿Desea Borrar El Lugar Seleccionado?","Si","No");
            if (!rta) return;
            using (var datos = new DataAccessLugares())
            {
                datos.DeleteLugarInteres(lugares);
            }

            await DisplayAlert("Confirmacion", "Lugar Seleccionado Borrado Correctamente", "Aceptar");
            Application.Current.MainPage = new NavigationPage(new LugaresInteresAdministrar());
        }

        private void BtnVolver_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new LugaresInteresAdministrar());
        }

        public async void Validaciones()
        {
            if (String.IsNullOrEmpty(NombreLugar.Text))
            {
                await DisplayAlert("Error", "Debe Ingresar un Nombre", "Aceptar");
                NombreLugar.Focus();
                return;
            }

            if (String.IsNullOrEmpty(Descripcion.Text))
            {
                await DisplayAlert("Error", "Debe Ingresar una Descripcion", "Aceptar");
                Descripcion.Focus();
                return;
            }


            if (String.IsNullOrEmpty(Tipo.Text))
            {
                await DisplayAlert("Error", "Debe Ingresar un Tipo de Lugar", "Aceptar");
                Tipo.Focus();
                return;
            }

            if (String.IsNullOrEmpty(Latidud.Text))
            {
                await DisplayAlert("Error", "Debe Ingresar una Latitud Valida", "Aceptar");
                Latidud.Focus();
                return;
            }

            if (String.IsNullOrEmpty(Longitud.Text))
            {
                await DisplayAlert("Error", "Debe Ingresar una Longitud Valida", "Aceptar");
                Longitud.Focus();
                return;
            }
        }

    }
}