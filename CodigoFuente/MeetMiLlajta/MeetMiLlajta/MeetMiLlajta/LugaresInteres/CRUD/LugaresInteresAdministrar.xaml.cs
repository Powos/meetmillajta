﻿using MeetMiLlajta.BaseDatos;
using MeetMiLlajta.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MeetMiLlajta.LugaresInteres.CRUD
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LugaresInteresAdministrar : ContentPage
    {
        public LugaresInteresAdministrar()
        {
            InitializeComponent();
            LugaresList.ItemTemplate = new DataTemplate(typeof(LugarInteresCell));
            using (var datos = new DataAccessLugares()) {
                LugaresList.ItemsSource = datos.getLugaresInteres();
            }
        }

        private async void BtnAgregar_Clicked(object sender, EventArgs e)
        {
            Validaciones();

            BaseDatos.LugaresInteres lugar = new BaseDatos.LugaresInteres
            {
                Nombre = NombreLugar.Text,
                Descripcion = Descripcion.Text,
                Tipo = Tipo.Text,
                Latitud = Latidud.Text,
                Longitud = Longitud.Text
            };

            using (var datos = new DataAccessLugares())
            {
                datos.InsertLugarInteres(lugar);
                LugaresList.ItemsSource = datos.getLugaresInteres();
            }

            NombreLugar.Text = string.Empty;
            Descripcion.Text = string.Empty;
            Tipo.Text = string.Empty;
            Latidud.Text = string.Empty;
            Longitud.Text = string.Empty;

            await DisplayAlert("Mensaje", "Lugar de Interes Creado con Exito", "Aceptar");

        }

        private void BtnVolver_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new PerfilUsuario());
        }

        private void LugaresList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new LugaresInteresEditar((BaseDatos.LugaresInteres)e.SelectedItem));
        }

        public async void Validaciones() {
            if (String.IsNullOrEmpty(NombreLugar.Text))
            {
                await DisplayAlert("Error", "Debe Ingresar un Nombre", "Aceptar");
                NombreLugar.Focus();
                return;
            }

            if (String.IsNullOrEmpty(Descripcion.Text))
            {
                await DisplayAlert("Error", "Debe Ingresar una Descripcion", "Aceptar");
                Descripcion.Focus();
                return;
            }

            if (String.IsNullOrEmpty(Tipo.Text))
            {
                await DisplayAlert("Error", "Debe Ingresar un Tipo de Lugar", "Aceptar");
                Tipo.Focus();
                return;
            }

            if (String.IsNullOrEmpty(Latidud.Text))
            {
                await DisplayAlert("Error", "Debe Ingresar una Latitud Valida", "Aceptar");
                Latidud.Focus();
                return;
            }

            if (String.IsNullOrEmpty(Longitud.Text))
            {
                await DisplayAlert("Error", "Debe Ingresar una Longitud Valida", "Aceptar");
                Longitud.Focus();
                return;
            }
        }
    }
}