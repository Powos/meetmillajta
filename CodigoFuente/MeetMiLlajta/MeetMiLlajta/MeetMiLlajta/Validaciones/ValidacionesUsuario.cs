﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeetMiLlajta.Validaciones
{
    public static class ValidacionesUsuario
    {
        public static bool ValidarNombreCompleto(string nombre, string apellido)
        {
            bool verificado = false;
            for (int i = 0; i < nombre.Length; i++)
            {
                if (char.IsLetter(nombre[i]))
                {
                    verificado = true;
                }
                else
                    verificado = false;
            }
            for (int i = 0; i < apellido.Length; i++)
            {
                if (char.IsLetter(apellido[i]) || char.IsWhiteSpace(apellido[i]))
                {
                    verificado = true;
                }
                else
                    verificado = false;
            }
            return verificado;
        }

        public static bool ValidarCorreo(string correo)
        {
            bool verificado = false;
            if (correo.Contains("@"))
            {
                if (correo.Contains("."))
                {
                    if (correo.Substring(correo.Length - 3) == "com")
                    {
                        verificado = true;
                    }
                }
            }
            return verificado;
        }

        public static bool VerificarContraseña(string contraseña, string confContraseña)
        {
            bool verificado = false;
            if (contraseña.Equals(confContraseña))
            {
                verificado = true;
            }
            return verificado;
        }
    }
}
