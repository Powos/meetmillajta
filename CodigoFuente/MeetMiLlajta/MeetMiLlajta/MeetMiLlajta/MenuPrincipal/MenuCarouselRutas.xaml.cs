﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MeetMiLlajta.MenuPrincipal
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuCarouselRutas : ContentPage
	{
        int invi;
        public MenuCarouselRutas(int invitado)
		{
			InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            invi = invitado;
        }

        public MenuCarouselRutas()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void BtnRutas_Clicked(object sender, EventArgs e)
        {
            if (invi == 1)
            {
                Application.Current.MainPage = new NavigationPage(new Usuario.InvitadoError());
            }
            else
            {
                Application.Current.MainPage = new NavigationPage(new Mapa.MapaInterfaz());
            }
        }

        private void BtnPerfil_Clicked(object sender, EventArgs e)
        {
            if (invi == 1)
            {
                btnPerfil.IsVisible = false;
            }
            else
            {
                Application.Current.MainPage = new NavigationPage(new Usuario.PerfilUsuario());
            }
        }
    }
}