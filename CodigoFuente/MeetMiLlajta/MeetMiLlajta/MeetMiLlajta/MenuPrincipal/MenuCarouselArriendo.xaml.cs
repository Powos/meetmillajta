﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MeetMiLlajta.MenuPrincipal
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuCarouselArriendo : ContentPage
	{
        int invi;
        public MenuCarouselArriendo(int invitado)
		{
			InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            invi = invitado;
        }
        public MenuCarouselArriendo()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void BtnArriendo_Clicked(object sender, EventArgs e)
        {
            if (invi == 1)
            {
                Application.Current.MainPage = new NavigationPage(new Arriendo.BusquedadeArriendoInterfaz());
            }
            else
            {
                Application.Current.MainPage = new NavigationPage(new Arriendo.BusquedadeArriendoInterfaz());
            }
            
        }

        private void BtnPerfil_Clicked(object sender, EventArgs e)
        {
            if (invi == 1)
            {
                btnPerfil.IsVisible = false;
            }
            else
            {
                Application.Current.MainPage = new NavigationPage(new Usuario.PerfilUsuario());
            }
            
        }
    }
}