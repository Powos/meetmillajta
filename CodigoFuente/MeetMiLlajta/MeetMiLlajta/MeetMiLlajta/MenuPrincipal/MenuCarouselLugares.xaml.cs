﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MeetMiLlajta.MenuPrincipal
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuCarouselLugares : ContentPage
	{
        int invi;
        public MenuCarouselLugares(int invitado)
		{
			InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            invi = invitado;
        }
        public MenuCarouselLugares()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void BtnLugares_Clicked(object sender, EventArgs e)
        {
            if (invi == 1)
            {
                Application.Current.MainPage = new NavigationPage(new Usuario.InvitadoError());
            }
            else
            {
                //Application.Current.MainPage = new NavigationPage(new LugaresInteres.LugaresInferfaz());
                Application.Current.MainPage = new NavigationPage(new Mapa.LugaresInteresMapa());
            }
            
        }

        private void BtnPerfil_Clicked(object sender, EventArgs e)
        {
            if (invi == 1)
            {
                btnPerfil.IsVisible = false;
            }
            else
            {
                Application.Current.MainPage = new NavigationPage(new Usuario.PerfilUsuario());
            }
        }
    }
}