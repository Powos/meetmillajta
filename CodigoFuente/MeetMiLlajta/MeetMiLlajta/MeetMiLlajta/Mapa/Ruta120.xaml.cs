﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace MeetMiLlajta.Mapa
{
	public partial class Ruta120 : ContentPage
	{
		public Ruta120 ()
		{
			InitializeComponent ();
            customMap.RouteCoordinates.Clear();
            customMap.linea120();
            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(-17.364489, -66.178296),
                                    Distance.FromMiles(3)));

            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void BtnVolver_Clicked(object sender, EventArgs e)
        {
            CarouselPage CRP = new CarouselPage();
            Application.Current.MainPage = CRP;
            CRP.Children.Add(new MenuPrincipal.MenuCarouselArriendo());
            CRP.Children.Add(new MenuPrincipal.MenuCarouselLugares());
            CRP.Children.Add(new MenuPrincipal.MenuCarouselRutas());
        }

        private void BtnRuta106_Clicked(object sender, EventArgs e)
        {

            Application.Current.MainPage = new NavigationPage(new Mapa.Ruta106());


        }

        private void BtnRuta150_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new Mapa.Ruta150());
        }
    }
}