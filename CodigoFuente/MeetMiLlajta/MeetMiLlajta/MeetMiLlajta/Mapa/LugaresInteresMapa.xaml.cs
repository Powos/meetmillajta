﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace MeetMiLlajta.Mapa
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LugaresInteresMapa : ContentPage
	{
        
		public LugaresInteresMapa ()
		{
			InitializeComponent ();
            MapView.MoveToRegion(
                MapSpan.FromCenterAndRadius(new Position(-17.3324766, -66.2239907), Distance.FromMiles(1)));
            NavigationPage.SetHasNavigationBar(this, false);
            var position1 = new Position(-17.332626, -66.225968);
            var position2 = new Position(-17.336431, -66.223427);
            var position3 = new Position(-17.336357, -66.226358);

            var pin1 = new Pin
            {
                Type = PinType.Place,
                Position = position1,
                Label = "Pizzeria Rica",
                Address = "Las Mejores Pizzas de Tiquipaya"
            };

            var pin2 = new Pin
            {
                Type = PinType.Place,
                Position = position2,
                Label = "Tienda La case",
                Address = "Abarrotes bebidas y Otros"
            };

            var pin3 = new Pin
            {
                Type = PinType.Place,
                Position = position3,
                Label = "Pension doña Julia",
                Address = "Como la comida de casa"
            };

            MapView.Pins.Add(pin1);
            MapView.Pins.Add(pin2);
            MapView.Pins.Add(pin3);
        }

        private void BtnVolver_Clicked(object sender, EventArgs e)
        {
            //Application.Current.MainPage = new NavigationPage(new LugaresInteres.LugaresInferfaz());
            CarouselPage CRP = new CarouselPage();
            Application.Current.MainPage = CRP;
            CRP.Children.Add(new MenuPrincipal.MenuCarouselArriendo());
            CRP.Children.Add(new MenuPrincipal.MenuCarouselLugares());
            CRP.Children.Add(new MenuPrincipal.MenuCarouselRutas());
        }
    }
}