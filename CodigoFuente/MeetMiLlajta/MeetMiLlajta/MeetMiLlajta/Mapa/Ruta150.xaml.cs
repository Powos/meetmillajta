﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace MeetMiLlajta.Mapa
{
	public partial class Ruta150 : ContentPage
	{
		public Ruta150 ()
		{
			InitializeComponent ();
            customMap.linea150();
            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(-17.355346, -66.227038),
                                    Distance.FromMiles(3)));

            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void BtnVolver_Clicked(object sender, EventArgs e)
        {
            CarouselPage CRP = new CarouselPage();
            Application.Current.MainPage = CRP;
            CRP.Children.Add(new MenuPrincipal.MenuCarouselArriendo());
            CRP.Children.Add(new MenuPrincipal.MenuCarouselLugares());
            CRP.Children.Add(new MenuPrincipal.MenuCarouselRutas());
        }

        private void BtnRuta106_Clicked(object sender, EventArgs e)
        {

            Application.Current.MainPage = new NavigationPage(new Mapa.Ruta106());


        }

        private void BtnRuta120_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new Mapa.Ruta120());
        }
    }
}