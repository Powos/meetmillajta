USE [master]
GO
/****** Object:  Database [BDPrueba]    Script Date: 15/09/2018 9:50:05 ******/
CREATE DATABASE [BDPrueba]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BDPrueba', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\BDPrueba.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BDPrueba_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\BDPrueba_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BDPrueba] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BDPrueba].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BDPrueba] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BDPrueba] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BDPrueba] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BDPrueba] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BDPrueba] SET ARITHABORT OFF 
GO
ALTER DATABASE [BDPrueba] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BDPrueba] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BDPrueba] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BDPrueba] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BDPrueba] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BDPrueba] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BDPrueba] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BDPrueba] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BDPrueba] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BDPrueba] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BDPrueba] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BDPrueba] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BDPrueba] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BDPrueba] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BDPrueba] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BDPrueba] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BDPrueba] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BDPrueba] SET RECOVERY FULL 
GO
ALTER DATABASE [BDPrueba] SET  MULTI_USER 
GO
ALTER DATABASE [BDPrueba] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BDPrueba] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BDPrueba] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BDPrueba] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [BDPrueba] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'BDPrueba', N'ON'
GO
USE [BDPrueba]
GO
/****** Object:  Table [dbo].[Administrador]    Script Date: 15/09/2018 9:50:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Administrador](
	[idAdministrador] [int] NOT NULL,
	[codigoAdministrador] [varchar](50) NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_Administrador] PRIMARY KEY CLUSTERED 
(
	[idAdministrador] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Arrendatario]    Script Date: 15/09/2018 9:50:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Arrendatario](
	[idArrendatario] [int] NOT NULL,
	[ci] [varchar](60) NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_Arrendatario] PRIMARY KEY CLUSTERED 
(
	[idArrendatario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Arriendo]    Script Date: 15/09/2018 9:50:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Arriendo](
	[idArriendo] [int] IDENTITY(1,1) NOT NULL,
	[direccion] [varchar](100) NOT NULL,
	[costo] [int] NOT NULL,
	[urlImagen] [varchar](100) NOT NULL,
	[codigoArriendo] [varchar](50) NOT NULL,
	[idArrendatario] [int] NOT NULL,
	[idAdministrador] [int] NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_Arriendo] PRIMARY KEY CLUSTERED 
(
	[idArriendo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Descripcion]    Script Date: 15/09/2018 9:50:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Descripcion](
	[idDescripcion] [int] IDENTITY(1,1) NOT NULL,
	[nroCuartos] [tinyint] NOT NULL,
	[nroBaños] [tinyint] NOT NULL,
	[TipoArriendo] [varchar](50) NOT NULL,
	[tamaño] [int] NOT NULL,
	[extras] [tinyint] NOT NULL,
 CONSTRAINT [PK_Descripcion] PRIMARY KEY CLUSTERED 
(
	[idDescripcion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LugaresInteres]    Script Date: 15/09/2018 9:50:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LugaresInteres](
	[idLugaresInteres] [int] IDENTITY(1,1) NOT NULL,
	[nombreLugar] [varchar](80) NOT NULL,
	[tipo] [varchar](70) NOT NULL,
	[estado] [nchar](10) NOT NULL,
	[latitud] [float] NOT NULL,
	[longitud] [float] NOT NULL,
	[idAdministrador] [int] NOT NULL,
	[idUsuario] [int] NOT NULL,
 CONSTRAINT [PK_LugaresInteres] PRIMARY KEY CLUSTERED 
(
	[idLugaresInteres] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Rutas]    Script Date: 15/09/2018 9:50:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rutas](
	[idRuta] [int] IDENTITY(1,1) NOT NULL,
	[latitudInicio] [float] NOT NULL,
	[longitudInicio] [float] NOT NULL,
	[latitudFinal] [float] NOT NULL,
	[longitudFinal] [float] NOT NULL,
	[idAdministrador] [int] NOT NULL,
	[idUsuario] [int] NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_Rutas] PRIMARY KEY CLUSTERED 
(
	[idRuta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 15/09/2018 9:50:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[idUsuario] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](60) NOT NULL,
	[apellidos] [varchar](80) NOT NULL,
	[correo] [varchar](100) NOT NULL,
	[nombreUsuario] [varchar](60) NOT NULL,
	[contraseña] [varbinary](50) NOT NULL,
	[rol] [varchar](50) NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Arriendo]  WITH CHECK ADD  CONSTRAINT [FK_Arriendo_Administrador] FOREIGN KEY([idAdministrador])
REFERENCES [dbo].[Administrador] ([idAdministrador])
GO
ALTER TABLE [dbo].[Arriendo] CHECK CONSTRAINT [FK_Arriendo_Administrador]
GO
ALTER TABLE [dbo].[Arriendo]  WITH CHECK ADD  CONSTRAINT [FK_Arriendo_Arrendatario] FOREIGN KEY([idArrendatario])
REFERENCES [dbo].[Arrendatario] ([idArrendatario])
GO
ALTER TABLE [dbo].[Arriendo] CHECK CONSTRAINT [FK_Arriendo_Arrendatario]
GO
ALTER TABLE [dbo].[Arriendo]  WITH CHECK ADD  CONSTRAINT [FK_Arriendo_Descripcion] FOREIGN KEY([idArriendo])
REFERENCES [dbo].[Descripcion] ([idDescripcion])
GO
ALTER TABLE [dbo].[Arriendo] CHECK CONSTRAINT [FK_Arriendo_Descripcion]
GO
ALTER TABLE [dbo].[LugaresInteres]  WITH CHECK ADD  CONSTRAINT [FK_LugaresInteres_Administrador] FOREIGN KEY([idAdministrador])
REFERENCES [dbo].[Administrador] ([idAdministrador])
GO
ALTER TABLE [dbo].[LugaresInteres] CHECK CONSTRAINT [FK_LugaresInteres_Administrador]
GO
ALTER TABLE [dbo].[LugaresInteres]  WITH CHECK ADD  CONSTRAINT [FK_LugaresInteres_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[LugaresInteres] CHECK CONSTRAINT [FK_LugaresInteres_Usuario]
GO
ALTER TABLE [dbo].[Rutas]  WITH CHECK ADD  CONSTRAINT [FK_Rutas_Administrador] FOREIGN KEY([idAdministrador])
REFERENCES [dbo].[Administrador] ([idAdministrador])
GO
ALTER TABLE [dbo].[Rutas] CHECK CONSTRAINT [FK_Rutas_Administrador]
GO
ALTER TABLE [dbo].[Rutas]  WITH CHECK ADD  CONSTRAINT [FK_Rutas_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[Rutas] CHECK CONSTRAINT [FK_Rutas_Usuario]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Administrador] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Administrador] ([idAdministrador])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Administrador]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Arrendatario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Arrendatario] ([idArrendatario])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Arrendatario]
GO
USE [master]
GO
ALTER DATABASE [BDPrueba] SET  READ_WRITE 
GO
